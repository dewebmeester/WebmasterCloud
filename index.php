<?php include 'includes/db.php';
    ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="stylesheet" href="bootstrap.css">
    <script src="bootstrap.js"></script>
    <script src="jquery-3.1.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <title>CMS Localhost</title>

</head>
<body>

<header class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <a href="index.php" class="navbar-brand">CMS System</a>
        <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Contact<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </li>
            <li><a href="http://localhost/AdminPanel/admin/login" target="_blank">Login</a></li>
        </ul>

    </div>
</header>
<div class="container">
    <article class="row">
    <section class="col-lg-8">
        <?php
        $query = "SELECT * FROM posts";
        $run_sql = mysqli_query($conn, $query);
        while ($rows = mysqli_fetch_assoc($run_sql)){
            echo '
			<div class="panel panel-default">
        <div class="panel-body">
            <div class="panel-heading">
                <h2>Content Management System</h2>
            </div>
            <img align="right" style="position: static; left: 50%" src="images\picture1.jpg">
            <p>Zodra de heer Timo Vaassen meer heeft geleerd over het vak Web komt hier meer informatie te staan.</p>
        </div>

    </div>
            ';
        }
?>
    
    </section>
    <aside class="col-lg-4">
        <form class="panel-group form-horizontal" role="form">
		<div class="panel panel-default">
                <div class="panel-heading">Login Area</div>
                <div class="panel-body">
					<div class="panel-header">
					<h4>Search Something</h4>
					</div>
					<div class="input-group">
					<input type="search" class="form-control" placeholder="Seacht Something...">
					<div class="input-group-btn">
					<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					</div>
					</div>
					</div>
					</div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="username" class="control-label col-sm-4">Username</label>
                        <div class="col-lg-7">
                            <input type="text" id="username" placeholder="Insert Email adress" class="form-control">
                        </div>
                </div>
                    <div class="form-group">
                        <label for="password" class="control-label col-sm-4">Password</label>
                        <div class="col-lg-7">
                            <input type="password" id="password" placeholder="Insert Password" class="form-control">
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-success btn-block">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    <h4 class="list-group-item-heading"> List item 1</h4>
                    <p class="list-group-item-text"> Dit CMS System is baas!</p>
				</a>
				<a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading"> List item 1</h4>
                    <p class="list-group-item-text"> Dit CMS System is baas!</p>
				</a>
				<a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading"> List item 1</h4>
                    <p class="list-group-item-text"> Dit CMS System is baas!</p>
				</a>
				<a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading"> List item 1</h4>
                    <p class="list-group-item-text"> Dit CMS System is baas!</p>
				</a>
				<a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading"> List item 1</h4>
                    <p class="list-group-item-text"> Dit CMS System is baas!</p>
				</a>
            </div>
        </aside>
    </article>
</div>
<footer class="navbar navbar-default navbar-fixed-bottom">
<div class="container">
<p class="navbar-text pull-left">Created By Timo Vaassen</p>
<a href="#" class="btn btn-primary pull-right navbar-btn">Share</a>
</div>
</body>
</html>